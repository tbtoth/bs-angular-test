//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Require gulp plugins with plugin loader.
// Pros: no need to specify, cons: anti pattern names, no explicitly named.
// can be used with $. prefix
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var run = require('run-sequence');

var gulp = require('gulp'),
  $ = require('gulp-load-plugins')({
    pattern: [
      'gulp-*',
      'gulp.*',
      'del'
    ]
  });

// Path settings for Gulp
var config = {
  bowerInstallPaths: [
    'app/vendor'
  ],
  bowerPaths: [
    'bower.json'
  ]
};

// Gulp task to install bower packages
gulp.task('bower-install', function () {
  return gulp.src(config.bowerPaths)
    .pipe($.debug())
    .pipe($.install({
      args: ['--config.interactive=false']
    }))
    .pipe($.notify({
      onLast: true,
      message: 'Installed all bower packages to their separated locations!'
    }));
});

// Gulp task to remove all bower_components directories
gulp.task('bower-clean', function (cb) {
  return $.del(config.bowerInstallPaths, cb);
});

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var __basicDirName = './app';
var __listenPort = '3200';

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
 * Default static file server for development.
 */
gulp.task('serve', function () {
  $.connect.server({
    root: __basicDirName,
    port: __listenPort
  });
});

gulp.task('lint', function () {
  return gulp.src(['app/js/**/*.js'])
    // eslint() attaches the lint output to the eslint property
    // of the file object so it can be used by other modules.
    .pipe($.eslint({
      'extends': 'eslint:recommended'
    }))
    // eslint.format() outputs the lint results to the console.
    // Alternatively use eslint.formatEach() (see Docs).
    //.pipe(eslint.formatEach('compact', process.stderr))
    .pipe($.eslint.format())
    // To have the process exit with an error code (1) on
    // lint error, return the stream and pipe to failOnError last.
    .pipe($.eslint.failOnError());
});

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var __distDirName = 'dist/';
var __distListenPort = '3300';

/**
 * The dist starter.
 */
gulp.task('dist', function () {
  return run(
    'dist:clean', // before all, clean dist dir
    // Useref
    [
      'dist:core',
      'dist:asset-copy',
      'dist:html2js'
    ],
    [
      'dist:html-replace'
    ],
    function (error) {
      if (error) {
        $.util.log($.util.colors.red('Dist task finished with error!', error));
        $.notify({
          message: 'Error in dist task!'
        });
      } else {
        $.util.log($.util.colors.green('Dist task finished successfully!'));
        $.notify({
          message: 'Dist task finished successfully!'
        });
      }
    });
});

/**
 * Dist demo serve
 */
gulp.task('serve-dist', function () {
    $.connect.server({
        root: __distDirName,
        port: __distListenPort
    });
});


/**
 * Clean the dist dir
 */
gulp.task('dist:clean', function () {
  return gulp.src(__distDirName, {
    read: false
  })
    .pipe($.clean({
      force: true
    })
  );
});

/**
 * Useref with js and css processors
 */
gulp.task('dist:core', function () {
  return gulp.src(__basicDirName + '/index.html')
    .pipe($.useref({}))
    .pipe($.if('*.js', $.ngAnnotate()))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.minifyCss()))
    .pipe(gulp.dest(__distDirName));
});

/**
 * Copy assets
 */
gulp.task('dist:asset-copy', function () {
  return gulp.src([
    __basicDirName + '/fonts/**'
  ], {
    base: __basicDirName
  })
    .pipe(gulp.dest(__distDirName + 'assets'));
});

gulp.task('dist:html2js', function () {
  return gulp.src(__basicDirName + '/view/**/*.html')
    .pipe($.ngHtml2js({
      moduleName: 'gsApp',
      prefix: '../view/'
    }))
    .pipe($.concat('view-partials.min.js'))
    //.pipe($.uglify())
    .pipe(gulp.dest('dist/partials'));
});

gulp.task('dist:html-replace', function () {
  // Process HTML and set block replacement
  return gulp.src('dist/index.html')
    .pipe($.htmlReplace({
      'partials': [
        'partials/view-partials.min.js'
      ]
    }))
    .pipe(gulp.dest(__distDirName)
  );
});