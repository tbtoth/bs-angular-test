# BS Angular Test SOLUTION NOTES

This applications is a SPA with three tabs:

* a dashboard tab with generic details about the selected repository
* an issue list tab
* a visualization tab

# IMPORTANT: Version Mismatch NOTE
As you can see, this app built up on angular 1.x. Before the test task changed on 2016.december, I have been already contacted once 
by BlackSwan in early June 2016 and started to work on this very instance (task v1). I was halfway done with the implementation when HR 
reported back that they have already filled the position, so the process stuck in a half.
When BlackSwan reached out to me this time (2017.january) I was able to continue my assignment and did not want to start over.
The structure and mechanism was designed by angular 1.x logic, so there were no reasons behind an angular 2.x wrapping, so I let it with 
the original framework and finished the work.
If you need me to rewrite it from scratch in angular 2.x, please make a notice, but I am sure this code suggests my coding style well, despite
half of it were made six months ago.

## Installation, dependencies
As a standard frontend application it ships with a raw developer suite via gulp.
So this version depends on npm. On the other hand there is a distilled version that can work out of the box,
but it's for 'production' use, so it may contain concatenated, minimized, uglified resources.
The main difference is about the html files, for a dev server the gulp servers static content, but for a raw run it is
pre-compiled for a ng-template js, so no webserver needed for a dist demonstration.
First of all, you have to check out it from git.

### Developer
For the dev mode install: node (tested at 7.4) and npm. Of course, npm and node command have to be accessible (so be in the PATH).
The lifecycle tasks are managed by gulp (local dependency, not global).

#### (re)Install steps

* `rm -rf node_modules/`
* `rm -rf app/vendor/`
* `npm install`
* `npm run install`

[Note: in a clean project clone 'npm install' then 'npm run install' should do]

Finally, to run the application call from the project root:
`npm run start`
and open the application in the browser:

**http://localhost:3200**

### Dist
For a quick look or a dist version test just open the dist/index.html file in your latest Chrome browser.
If you want a redist then install like specified above (Developer section) then run
`npm run dist`

### Dist test
The distilled version also can be tested
`npm run start-dist`
and open the production ready application in the browser:

**http://localhost:3300**

### Tested on
Mac + latest Chrome @wqhd and @fhd

## For production...TODOs

* unit testing (for at least every repository and service)
* E2E testing (for at least every basic user case)