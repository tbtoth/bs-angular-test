(function () {
  'use strict';

  /**
   * Details wrapper directive with controller
   */
  angular
    .module('gs.components.repositorydetails')
    .directive('repositoryDetails', repositoryDetails);

  repositoryDetails.$inject = ['CONFIG'];

  function repositoryDetails(CONFIG) {

    // Directive object
    return {
      restrict: 'AEC',
      scope: {},
      templateUrl: CONFIG.TEMPLATE["repository-details"],
      controller: repositoryDetailsController,
      link: link
    };

    function link($scope, element, attr) {

    }
  }

  repositoryDetailsController.$inject = ['$scope', 'EVENT'];
  function repositoryDetailsController($scope, EVENT) {
    // Event listeners
    $scope.$on(EVENT.REPOSITORY_SELECTED, repositoryChangedHandler);

    // Attached to template
    $scope.state = {
      opened: false,
      active: 0
    };

    $scope.data = {
      selectedRepository: null
    };

    // Template callable functions
    $scope.controlFunctions = {
      selectTab: selectTab
    };

    /**
     * Selected repository on change event handler
     * @param $event
     * @param repository
     */
    function repositoryChangedHandler($event, repository) {
      $scope.data.selectedRepository = repository;
      $scope.state.opened = !!repository;
    }

    /**
     * Forced tab setter
     * @param tabIndex
     */
    function selectTab(tabIndex) {
      if (angular.isNumber(tabIndex)) {
        $scope.state.active = tabIndex;
      }
    }

  }

})();
