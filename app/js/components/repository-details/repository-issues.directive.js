(function () {
    'use strict';

    /**
     * Repository issues directive with controller
     */
    angular
        .module('gs.components.repositorydetails')
        .directive('repositoryIssues', repositoryIssues);

    repositoryIssues.$inject = ['CONFIG'];

    function repositoryIssues(CONFIG) {

        // Directive object
        return {
            restrict: 'AEC',
            scope: {
                repository: '=',
                controlFunctions: '='
            },
            templateUrl: CONFIG.TEMPLATE["repository-issues"],
            controller: repositoryIssuesController
        };

    }

    repositoryIssuesController.$inject = ['$scope', 'dataAccessService', 'EVENT'];
    function repositoryIssuesController($scope, dataAccessService, EVENT) {
        // Event listeners
        $scope.$on(EVENT.REPOSITORY_SELECTED, repositoryChangedHandler);

        // Table, same syntax like controllerAs
        $scope.mc = {
            displayed: [],
            numberOfPages: 0,
            totalElement: 0,
            itemsByPage: {},
            callServer: callServer
        };

        /**
         * Change event handler
         * @param $event
         * @param repository
         */
        function repositoryChangedHandler($event, repository) {
            $scope.repository = repository;
            // Force table refresh
            $scope.mc.displayed = [];
            $scope.mc.numberOfPages = 0;
            $scope.mc.totalElement = 0;
            var tableState = {
                sort: {},
                search: {},
                pagination: {
                    start: 0,
                    number: 20,
                    numberOfPages: 0
                }
            };
            callServer(tableState);
        }

        /**
         * Pipe for the smart table
         * @param tableState
         */
        function callServer(tableState) {
            $scope.mc.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start || 0;
            var number = pagination.number || 20;
            dataAccessService.getIssuesByRepositoryAndPage(start, number, tableState, $scope.repository.full_name).then(function (result) {
                // Update table specific data
                $scope.mc.displayed = result.data;
                $scope.mc.totalElement = result.totalElement;
                $scope.mc.numberOfPages = result.numberOfPages;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalElement;
                $scope.mc.itemsByPage = number;
                $scope.mc.isLoading = false;
            });

        }

    }

})();
