(function () {
  'use strict';

  /**
   * Details basic directive
   */
  angular
    .module('gs.components.repositorydetails')
    .directive('repositoryDetailsBasic', repositoryDetailsBasic);

  repositoryDetailsBasic.$inject = ['CONFIG'];

  function repositoryDetailsBasic(CONFIG) {

    // Directive object
    return {
      restrict: 'AEC',
      scope: {
        repository: '=',
        controlFunctions: '='
      },
      templateUrl: CONFIG.TEMPLATE["repository-details-basic"]
    };

  }
})();
