(function () {
    'use strict';

    /**
     * Visualization directive
     */
    angular
        .module('gs.components.repositoryvisualization')
        .directive('repositoryVisualization', repositoryVisualization);

    repositoryVisualization.$inject = ['CONFIG'];

    function repositoryVisualization(CONFIG) {

        // Directive object
        return {
            restrict: 'AEC',
            scope: {
                repository: '=',
                isActive: '='
            },
            templateUrl: CONFIG.TEMPLATE["repository-visualization"],
            controller: repositoryVisualizationController
        };

    }

    repositoryVisualizationController.$inject = ['$scope', '$timeout', 'EVENT'];
    function repositoryVisualizationController($scope, $timeout, EVENT) {

        $scope.data = {
            isReady: false
        };

        /**
         * NOTE: raphael.js (a morris-char.js dependency) has some odd behaviour when calculating
         * off screen element sizes, therefor we can only draw the graph when its container is displayed.
         * To track this we monitor if the this tab is active, and only perform reset when it is.
         * Also, a patch is included from SO.
         */
        patch();
        $scope.$watch('isActive', reset);
        $scope.$on(EVENT.REPOSITORY_SELECTED, repositoryChangedHandler);

        reset();
        /**
         * Selected repository on change event handler
         * @param $event
         * @param repository
         */
        function repositoryChangedHandler($event, repository) {
            $scope.data.isReady = false;
            reset();
        }

        function reset() {
            // Wait the digest, then enforce the re-draw via data refresh
            $timeout(function () {
                if ($scope.repository) {
                    $scope.data.donutChart = [
                        {label: 'Stars', value: $scope.repository.stargazers_count},
                        {label: 'Watchers', value: $scope.repository.watchers_count},
                        {label: 'Forks', value: $scope.repository.forks_count},
                        {label: 'Open issues', value: $scope.repository.open_issues_count}
                    ];
                }
                $scope.data.isReady = true;
            }, 100);
        }

    }

    /**
     * http://stackoverflow.com/questions/34006159/morris-js-donut-doesnt-render-in-bootstrap-tab
     */
    function patch() {
        Morris.Donut.prototype.resizeHandler = function () {
            this.timeoutId = null;
            if (this.el && this.el.width() > 0 && this.el.height() > 0) {
                this.raphael.setSize(this.el.width(), this.el.height());
                return this.redraw();
            }
            else return null;
        };
        Morris.Donut.prototype.setData = function (data) {
            var row;
            this.data = data;
            this.values = (function () {
                var _i, _len, _ref, _results;
                _ref = this.data;
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    row = _ref[_i];
                    _results.push(parseFloat(row.value));
                }
                return _results;
            }).call(this);
            if (this.el && this.el.width() > 0 && this.el.height() > 0) {
                return this.redraw();
            }
            else return null;
        };
    }

})();
