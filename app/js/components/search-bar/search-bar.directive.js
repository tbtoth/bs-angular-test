(function () {
  'use strict';

  /**
   * Search bar directive with controller
   */
  angular
    .module('gs.components.searchbar')
    .directive('searchBar', searchBar)
    .directive('typeaheadClickOpen', typeaheadClickOpen);

  searchBar.$inject = ['CONFIG'];

  function searchBar(CONFIG) {

    // Directive object
    return {
      restrict: 'AEC',
      scope: {},
      templateUrl: CONFIG.TEMPLATE["search-bar"],
      controller: searchBarController
    };

  }

  searchBarController.$inject = ['CONFIG', '$scope', 'dataAccessService', '$timeout', 'EVENT', '$rootScope'];
  function searchBarController(CONFIG, $scope, dataAccessService, $timeout, EVENT, $rootScope) {

    // Attached to template
    $scope.state = {
      typeaheadOpen: false,
      typeaheadNoResults: false,
      typeaheadHttpInProgress: false
    };

    $scope.data = {
      repositorySearchValue: '',
      lastRepositorySearchValue: '',
      repositories: []
    };

    // Callable from template
    $scope.controlFunctions = {
      refreshRepositories: refreshRepositories,
      selectRepository: selectRepository,
      repositorySearchFieldChanged: repositorySearchFieldChanged
    };

    // Implementations
    function refreshRepositories() {
      if (CONFIG.API.BASE && CONFIG.API.REPO) {
        if ($scope.data.repositorySearchValue.length >= 3) {
          $scope.state.typeaheadOpen = true;
          $scope.state.typeaheadHttpInProgress = true;
          dataAccessService.getRepositoriesByName($scope.data.repositorySearchValue)
            .then(
            function (response) {
              // Success
              $scope.data.repositories = _.take(response.data.items, 10);
            }
          ).finally(
            function () {
              clickSearchField();
            }
          );
        }
      }
    }

    function repositorySearchFieldChanged() {
      if ($scope.data.repositorySearchValue !== $scope.data.lastRepositorySearchValue) {
        $scope.data.repositories = [];
      }
    }

    function selectRepository($item) {
      $rootScope.$broadcast(EVENT.REPOSITORY_SELECTED, $item);
    }

    function clickSearchField() {
      $timeout(function () {
        angular.element('#search-field').click()
      }, 0);
    }

  }

  /**
   * http://stackoverflow.com/questions/20879773/how-angular-ui-typeahead-trigger-when-focus
   * @param $parse
   * @param $timeout
   * @returns {{restrict: string, require: string, link: Function}}
   */
  typeaheadClickOpen.$inject = ['$parse', '$timeout'];
  function typeaheadClickOpen($parse, $timeout) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function ($scope, elem) {
        function triggerFunc() {
          var ctrl = elem.controller('ngModel'),
            prev = ctrl.$modelValue || '';
          if (prev) {
            ctrl.$setViewValue('');
            $timeout(function () {
              ctrl.$setViewValue(prev);
            });
          } else {
            ctrl.$setViewValue(' ');
          }
        }

        elem.bind('click', triggerFunc);
      }
    }
  }

})();
