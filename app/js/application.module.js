(function (angular) {
    'use strict';

    /**
     * Module build up file.
     */
    angular.module('gs.components.searchbar', []);
    angular.module('gs.components.repositorydetails', []);
    angular.module('gs.components.repositoryvisualization', []);
    angular.module('gs.components', [
        'gs.components.searchbar',
        'gs.components.repositorydetails',
        'gs.components.repositoryvisualization'
    ]);

    angular.module('gs.constants', []);

    angular.module('gs.commons.data', []);
    angular.module('gs.commons', [
        'gs.commons.data'
    ]);

    // Gather dependencies
    angular.module('gsApp', [
        'gs.components',
        'gs.constants',
        'gs.commons',
        'ngAnimate',
        'ui.bootstrap', 'angular-loading-bar', 'smart-table', 'angular.morris'
    ]);

    // The base application configuration is in application-base.config

}(angular || {}));
