(function () {
  'use strict';

  /**
   * Collection of the event names and values
   */
  angular
    .module('gs.constants')
    .constant('EVENT', {
      "REPOSITORY_SELECTED": "repositorySelected"
    });
})();
