(function () {
  'use strict';

  /**
   * Collection of the config values
   */
  angular
    .module('gs.constants')
    .constant('CONFIG', {
      "COMPONENT": {
        "SEARCH-BAR": {}
      },
      "API": {
        "BASE": "https://api.github.com/search/",
        "REPO": "repositories?q=",
        "ISSUE": "issues?q=repo:"
      },
      "TEMPLATE": {
        "search-bar": "../view/search-bar/search-bar.template.html",
        "repository-details": "../view/repository-details/repository-details.template.html",
        "repository-issues": "../view/repository-details/repository-issues.template.html",
        "repository-visualization": "../view/repository-details/repository-visualization.template.html",
        "repository-details-basic": "../view/repository-details/repository-details-basic.template.html"
      }
    });
})();
