(function (angular) {
  'use strict';

  ApplicationBaseConfig.$inject = ['$compileProvider', 'cfpLoadingBarProvider'];
  function ApplicationBaseConfig($compileProvider, cfpLoadingBarProvider) {
    // Performance gain
    $compileProvider.debugInfoEnabled(false);
    cfpLoadingBarProvider.includeSpinner = false;
  }

  // Configure the module
  angular.module('gsApp').config(ApplicationBaseConfig);

  RunBlock.$inject = ['$log'];
  function RunBlock($log) {
    $log.debug('Run :: Application started!');
  }

  // Module entry point
  angular.module('gsApp').run(RunBlock);

}(angular));
