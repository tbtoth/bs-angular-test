(function () {
  'use strict';

  /**
   * The only place where we communicate with the http API directly
   */
  angular
    .module('gs.commons.data')
    .factory('dataAccessService', dataAccessService);

  dataAccessService.$inject = ['$http', '$q', 'CONFIG', '$log'];

  function dataAccessService($http, $q, CONFIG, $log) {

    //~~~~~~~~~~~~~~
    // API functions
    //~~~~~~~~~~~~~~
    /**
     * GETs on the endpoint for repositories by name.
     * @param name
     * @returns {*}
     */
    function getRepositoriesByName(name) {
      var deferred = $q.defer();
      if (name && CONFIG.API.BASE && CONFIG.API.REPO) {
        var uri = CONFIG.API.BASE + CONFIG.API.REPO + name + '&sort=stars&order=desc';
        $http.get(uri)
          .then(
          // Success
          function (response) {
            $log.info(
              'dataAccessService :: getRepositoriesByName call succeeded from URI: %s',
              uri
            );
            deferred.resolve(response);
          },
          // Fail
          function (e) {
            $log.warn('dataAccessService :: getRepositoriesByName server call error', e);
            deferred.reject('Server error!');
          })
      } else {
        $log.warn('dataAccessService :: getRepositoriesByName invalid params');
        deferred.reject('Bad parameter!');
      }
      return deferred.promise;
    }

    /**
     * GETS on the endpoint for issues by hard filter repo and paging
     * @param start
     * @param number
     * @param params
     * @param repository
     * @returns {*}
     */
    function getIssuesByRepositoryAndPage(start, number, params, repository) {
      var deferred = $q.defer();
      if (repository && CONFIG.API.BASE && CONFIG.API.ISSUE) {
        // Forge the fixed part
        var pageNumber = Math.floor(start / number);
        var pagerPart = '&page=' + pageNumber + '&per_page=' + number;
        // Get data
        var uri = CONFIG.API.BASE + CONFIG.API.ISSUE + repository + '&sort=created&order=desc' + pagerPart;
        $http.get(uri)
          .then(
          // Success
          function (response) {
            $log.info(
              'dataAccessService :: getIssuesByRepositoryAndPage call succeeded from URI: %s',
              uri
            );
            deferred.resolve({
              data: response.data.items,
              numberOfPages: Math.ceil(response.data.total_count / number),
              totalElement: response.data.total_count
            });
          },
          // Fail
          function (e) {
            $log.warn('dataAccessService :: getIssuesByRepositoryAndPage server call error', e);
            deferred.reject('Server error!');
          })
      } else {
        $log.warn('dataAccessService :: getIssuesByRepositoryAndPage invalid params');
        deferred.reject('Bad parameter!');
      }
      return deferred.promise;
    }

    // Service object
    return {
      getRepositoriesByName: getRepositoriesByName,
      getIssuesByRepositoryAndPage: getIssuesByRepositoryAndPage
    };

  }
})();
